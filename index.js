
// 1. What directive is used by Node.js in loading the modules it needs?
// Answer: It is the require() method



// 2. What Node.js module contains a method for server creation?

// Answer: It is the HTTP module



// 3. What is the method of the http object responsible for creating a server using Node.js?

// Answer: It is the createServer() method


// 4. What method of the response object allows us to set status codes and content types?

// Answer: It is the writeHead() method


// 5. Where will console.log() output its contents when run in Node.js?

// Answer: The output of console.log() run in Node.js appears on the specified port

// 6. What property of the request object contains the address's endpoint?

// Answer: It is called url


// Simple Server


const http = require('http')
const port = 3000

const server = http.createServer(function(request, response) {
	if (request.url == '/login') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to the login page.')
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'} )
		response.end("I'm sorry the page you are looking for cannot be found.")
	}
});

server.listen(port);

console.log(`Server is now accessible at localhost: ${port}`);